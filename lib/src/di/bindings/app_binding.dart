import 'package:dio/dio.dart';
import 'package:get/get.dart';

import 'package:Refalco/src/controllers/appController/app_controller.dart';
import 'package:Refalco/src/data/remote/api/clients/dio_client.dart';
import 'package:Refalco/src/data/remote/api/clients/rest_client.dart';
import 'package:Refalco/src/data/remote/api/app_apis.dart';
import 'package:Refalco/src/data/remote/constants/endpoints.dart';
import 'package:Refalco/src/data/repository.dart';
import 'package:Refalco/src/data/sheredpref/constants/preferences.dart';
import 'package:Refalco/src/data/sheredpref/shared_preference_helper.dart';
import 'package:Refalco/src/ui/screens/homePage/home_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../ui/screens/orderDetailsPage/order_details_controller.dart';

class AppBindings extends Bindings {


  AppBindings();

  @override
  void dependencies() {

    Get.put(SharedPreferenceHelper(Get.find()),permanent: true);

    Get.lazyPut<Dio>(() {
      final dio = Dio();
      dio
        ..options.baseUrl = Endpoints.baseUrl
        ..options.connectTimeout = Endpoints.connectionTimeout
        ..options.receiveTimeout = Endpoints.receiveTimeout
    // ..options.headers = {
    //   "Access-Control-Allow-Origin": "*", // Required for CORS support to work
    //   "Access-Control-Allow-Credentials": true, // Required for cookies, authorization headers with HTTPS
    //   "Access-Control-Allow-Headers": "Origin,Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token,locale",
    //   "Access-Control-Allow-Methods": "POST, OPTIONS"
    // }
//      ..options.responseType = ResponseType.bytes
        ..interceptors.add(LogInterceptor(
            request: true,
            responseBody: true,
            requestBody: true,
            requestHeader: true,
            error: true
        ))
        ..interceptors.add(
          InterceptorsWrapper(
            onRequest: (RequestOptions options,RequestInterceptorHandler handler) async {
              // getting shared pref instance
              var prefs = await SharedPreferences.getInstance();

              // getting token
              var token = prefs.getString(Preferences.token);
              // var token = null;
              print("tokenDioInstanse${token} ");

              if (token != null) {
                options.headers.putIfAbsent('Authorization', () => 'Bearer $token');
              } else {
                print('Auth token is null');
              }
              handler.next(options);
            },
              onError: (e, handler) {
                print(e.message);
                handler.next(e);
              },
              onResponse: (r, handler) {
                print(r.data);
                handler.next(r);
              }
          ),
        );

      return dio;
    },fenix: true);

    Get.lazyPut<DioClient>(() {
      return DioClient(Get.find());
    },fenix: true);

    Get.lazyPut<RestClient>(() {
      return RestClient();
    },fenix: true);

    Get.lazyPut<AppApis>(() {
      return AppApis(Get.find(),Get.find());
    },fenix: true);

    Get.lazyPut<Repository>(() {
      return Repository(Get.find(),Get.find());
    },fenix: true);

    Get.put(AppController(),permanent: true);

    Get.create<HomeController>((){
      return HomeController();
    });

    Get.create<OrderDetailsController>((){
      return OrderDetailsController();
    });

    //I added lazyPut for when I change language doesn't refresh and call the request.
    // Get.lazyPut<SelectLanguageController>(() {
    //   return SelectLanguageController(repository);
    // });


  }
}
