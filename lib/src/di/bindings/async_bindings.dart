import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:Refalco/src/data/sheredpref/shared_preference_helper.dart';
import 'package:shared_preferences/shared_preferences.dart';



class AsyncBindings extends Bindings {
  // final Repository repository;

  // final GlobalAppBloc globalAppBloc;

  AsyncBindings();

  @override
  Future dependencies() async{

   await Get.putAsync<SharedPreferences>(() async {

      return await SharedPreferences.getInstance();
    }, permanent: true); // Yes, we do get tag and permanent properties with this as well

   await Get.putAsync<SharedPreferenceHelper>(() async {

     return SharedPreferenceHelper(Get.find<SharedPreferences>());
   }, permanent: true); // Yes, we do get tag and permanent properties with this as well

  }
}
