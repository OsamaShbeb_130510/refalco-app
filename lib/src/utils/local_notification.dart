import 'package:flutter_local_notifications/flutter_local_notifications.dart';
class NotificationApi{
  static Future initialize(FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin)async{
    // return NotificationDetails(
    //   android: AndroidNotificationDetails(
    //     'channel id',
    //     'channel name',
    //     importance: Importance.max,
    //     channelDescription: 'channel description'
    //   ),
    //   iOS: DarwinNotificationDetails(),
    // );
    var androidInit=AndroidInitializationSettings('mipmap/ic_launcher');
    var iosInit = DarwinInitializationSettings();
    var initSettings=InitializationSettings(android: androidInit,iOS: iosInit);
    await flutterLocalNotificationsPlugin.initialize(initSettings);
  }
//   static final _notification=FlutterLocalNotificationsPlugin();
//   static Future showNotification({
//   int id=0,
//     String? title,
//     String? body,
//     String? payload,
// })
//   async => _notification.show(
//     id,
//     title,
//     body,
//     await notificationDetails(),
//     payload: payload,
//   );

static Future showText({id=0,required String title,required String body,
var payload,required FlutterLocalNotificationsPlugin fln
}) async{
    AndroidNotificationDetails androidNotificationDetails=
        new AndroidNotificationDetails(
            '2',
            'osama_channel',
          importance: Importance.max,
          priority: Priority.high,
        );
    var not=NotificationDetails(
      android: androidNotificationDetails,
      iOS: DarwinNotificationDetails(),
    );
    await fln.show(0,title,body,not);

}
}