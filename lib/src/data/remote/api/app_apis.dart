import 'dart:async';
import 'package:Refalco/src/data/models/orderModel/orders_response_model.dart';
import 'package:Refalco/src/data/models/test/movies_response.dart';
import 'package:Refalco/src/data/remote/api/clients/dio_client.dart';
import 'package:Refalco/src/data/remote/api/clients/rest_client.dart';
import 'package:Refalco/src/data/remote/constants/endpoints.dart';

class AppApis {
  // dio instance
  final DioClient _dioClient;

  // rest-client instance
  final RestClient _restClient;

  // injecting dio instance
  AppApis(this._dioClient, this._restClient);

  Future<OrdersResponse>  getOrders(int page,limit) async{
    final Map<String, dynamic> queryParameters = {
      'page': page,
      'limit': limit,
    };
    final res =await _dioClient.get(Endpoints.orders,queryParameters: queryParameters);
    return OrdersResponse.fromJson(res);
  }


}
