import 'dart:async';
// import 'package:shared_preferences/shared_preferences.dart';

import 'package:Refalco/src/data/models/user/user.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'constants/preferences.dart';

class SharedPreferenceHelper {
  final SharedPreferences _sharedPreference;

  SharedPreferenceHelper(this._sharedPreference);

  // General Methods: ----------------------------------------------------------


  Future<void> saveWishList(String wishList) async {
    _sharedPreference.setString(Preferences.wish, wishList);
  }
  Future<String> getWishList() async {
    return _sharedPreference.getString(Preferences.wish,)??'[]';
  }

  Future<void> saveOrderList(String orderList) async {
    _sharedPreference.setString(Preferences.orderList, orderList);
  }
  Future<String?> getOrderList() async {
    return _sharedPreference.getString(Preferences.orderList,);
  }

  Future<void> saveUser(UserModel user) async {

    user.userId != null
        ? _sharedPreference.setString(Preferences.userId, user.userId!)
        : {};
    user.firstName != null
        ? _sharedPreference.setString(Preferences.firsName, user.firstName!)
        : {};
    user.lastName != null
        ? _sharedPreference.setString(Preferences.lastName, user.lastName!)
        : {};
    user.email != null
        ? _sharedPreference.setString(Preferences.email, user.email!)
        : {};
    user.password != null
        ? _sharedPreference.setString(Preferences.password, user.password!)
        : {};
    user.phone != null
        ? _sharedPreference.setString(Preferences.phone, user.phone!)
        : {};
    user.socialId != null
        ? _sharedPreference.setString(Preferences.socialId, user.socialId!)
        : {};
    user.socialType != null
        ? _sharedPreference.setString(Preferences.socialType, user.socialType!)
        : {};
    user.photo != null
        ? _sharedPreference.setString(Preferences.photo, user.photo!)
        : {};
    user.bigPhoto != null
        ? _sharedPreference.setString(Preferences.bigPhoto, user.bigPhoto!)
        : {};
    user.active != null
        ? _sharedPreference.setBool(Preferences.active, user.active!)
        : {};
    user.token != null
        ? _sharedPreference.setString(Preferences.token, user.token!)
        : {};

  }
  Future<UserModel> getUser() async {
    return UserModel(
        userId: _sharedPreference.getString(Preferences.userId),
        firstName: _sharedPreference.getString(Preferences.firsName),
        lastName: _sharedPreference.getString(Preferences.lastName),
        password: _sharedPreference.getString(Preferences.password),
        email: _sharedPreference.getString(Preferences.email),
        phone: _sharedPreference.getString(Preferences.phone),
        socialId: _sharedPreference.getString(Preferences.socialId),
        socialType: _sharedPreference.getString(Preferences.socialType),
        token: _sharedPreference.getString(Preferences.token),
        photo: _sharedPreference.getString(Preferences.photo),
        bigPhoto: _sharedPreference.getString(Preferences.bigPhoto),
        createdAt: _sharedPreference.getString(Preferences.createdAt),
        active: _sharedPreference.getBool(Preferences.active),
      );
  }
  Future<void> removeUser() async {
    _sharedPreference.remove(Preferences.userId);
    _sharedPreference.remove(Preferences.firsName);
    _sharedPreference.remove(Preferences.lastName);
    _sharedPreference.remove(Preferences.email);
    _sharedPreference.remove(Preferences.phone);
    _sharedPreference.remove(Preferences.photo);
    _sharedPreference.remove(Preferences.bigPhoto);
    _sharedPreference.remove(Preferences.token);
    _sharedPreference.remove(Preferences.createdAt);
    _sharedPreference.remove(Preferences.socialId);
    _sharedPreference.remove(Preferences.socialType);
    _sharedPreference.remove(Preferences.password);
    _sharedPreference.remove(Preferences.active);
    // _sharedPreference.remove(Preferences.userClientToken);
      print('UserRemoved');
  }

  // Future<String> get odooAuthToken async {
  //   return _sharedPreference.then((preference) {
  //     return preference.getString(Preferences.odoo_auth_token)!;
  //   });
  // }
  //
  // Future<void> saveOdooAuthToken(String authToken) async {
  //   return _sharedPreference.then((preference) {
  //     preference.setString(Preferences.odoo_auth_token, authToken);
  //   });
  // }
  //
  // Future<void> removeOdooAuthToken() async {
  //   return _sharedPreference.then((preference) {
  //     preference.remove(Preferences.odoo_auth_token);
  //   });
  // }
  //
  // Future<int> get companyId async {
  //   return _sharedPreference.then((preference) {
  //     return preference.getInt(Preferences.companyId)!;
  //   });
  // }
  //
  // Future<void> saveCompanyId(int companyId) async {
  //   return _sharedPreference.then((preference) {
  //     preference.setInt(Preferences.companyId, companyId);
  //   });
  // }

//   Future<String> get authToken async {
//     return _sharedPreference.then((preference) {
//       return preference.getString(Preferences.auth_token);
//     });
//   }
//
//   Future<void> saveAuthToken(String authToken) async {
//     return _sharedPreference.then((preference) {
//       preference.setString(Preferences.auth_token, authToken);
//     });
//   }
//
//   Future<void> removeAuthToken() async {
//     return _sharedPreference.then((preference) {
//       preference.remove(Preferences.auth_token);
//     });
//   }
//
//   Future<bool> get isLoggedIn async {
//     return _sharedPreference.then((preference) {
//       return preference.getString(Preferences.auth_token) ?? false;
//     });
//   }
//
// /*
//   Theme:------------------------------------------------------
// */
//   Future<bool> get isDarkMode {
//     return _sharedPreference.then((prefs) {
//       return prefs.getBool(Preferences.is_dark_mode) ?? false;
//     });
//   }

  // Future<void> changeBrightnessToDark(bool value) {
  //   return _sharedPreference.then((prefs) {
  //     return prefs.setBool(Preferences.is_dark_mode, value);
  //   });
  // }
  //
  // // Language:---------------------------------------------------
  String get currentLanguage {
    return _sharedPreference.getString(Preferences.current_language) ?? 'en';
  }
  //
  Future<Locale> fetchLocale() async {
    Locale locale = Locale('en', 'US');
    print("Current_Language_preff ${_sharedPreference.getString('language_code')}");
    if (_sharedPreference.getString('language_code') == null) {
      locale = Locale('en', 'US');
      return locale;
    }
    locale = Locale(_sharedPreference.getString('language_code')!);
    return locale;
  }





  //
  // Future<void> changeLanguage(String language) {
  //   return _sharedPreference.then((prefs) {
  //     return prefs.setString(Preferences.current_language, language);
  //   });
  // }
}
