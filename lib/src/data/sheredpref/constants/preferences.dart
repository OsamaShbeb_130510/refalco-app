class Preferences {

  static const String userId = 'userId';
  static String firsName = 'firsName';

  static String lastName = 'lastName';

  static String password = 'password';

  static String email = 'email';

  static String phone = 'phone';

  static String socialId = 'socialId';

  static String socialType = 'socialType';

  static String token = 'token';

  static String photo = 'photo';

  static String bigPhoto = 'bigPhoto';

  static String createdAt = 'createdAt';

  static String active = 'active';

  static String wish = 'WishList';

  static String orderList = 'orderList';


  Preferences._();

  static const String is_logged_in = "isLoggedIn";
//  static const String auth_token = "authToken";
  static const String odoo_auth_token = "odoo_auth_token";
  static const String is_dark_mode = "is_dark_mode";
  static const String current_language = "current_language";


}