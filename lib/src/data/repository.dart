import 'dart:ui';
import 'package:Refalco/src/data/models/orderModel/orders_response_model.dart';
import 'package:Refalco/src/data/models/user/user.dart';
import 'package:Refalco/src/data/remote/api/app_apis.dart';
import 'package:Refalco/src/data/sheredpref/shared_preference_helper.dart';

class Repository {

  final AppApis appApis;

  final SharedPreferenceHelper _sharedPrefsHelper;

  Repository(this.appApis, this._sharedPrefsHelper,);

  /* --------------------------------API METHODS------------------------------------- */

  Future<OrdersResponse> getAllOrders(int page,limit) =>
      appApis.getOrders(page,limit);

  /* --------------------------------SHARED_PREFERENCES METHODS------------------------------------- */

  Future<Locale> get fetchLocale => _sharedPrefsHelper.fetchLocale();

  Future<String> get wishList => _sharedPrefsHelper.getWishList();

  Future<void> saveWishList(String wishList) => _sharedPrefsHelper.saveWishList(wishList);

  Future<String?> get orderList => _sharedPrefsHelper.getOrderList();

  Future<void> saveOrderList(String orderList) => _sharedPrefsHelper.saveOrderList(orderList);

  Future<UserModel> getUser() => _sharedPrefsHelper.getUser();

  Future<void> saveUser(UserModel user) => _sharedPrefsHelper.saveUser(user);

  Future<void> removeUser() => _sharedPrefsHelper.removeUser();


}
