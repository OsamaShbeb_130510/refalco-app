class UserModel {
  //Note UserId and Password I added it manually not from json response.
  String? userId;
  String? firstName = '';
  String? lastName = '';
  String? email;
  String? photo = '';
  String? bigPhoto;
  String? socialType;
  dynamic? socialId;
  String? phone;
  dynamic? active;
  String? createdAt;

  String? password;
  String? token;


  UserModel(
      {this.userId,
      this.email,
      this.firstName,
      this.lastName,
      this.password,
      this.phone,
      this.active,
      this.bigPhoto,
      this.photo,
      this.createdAt,
      this.socialId,
      this.socialType,
      this.token
      });

  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      userId: json['id'],
      firstName: json['first_name'],
      lastName: json['last_name'],
      email: json['email'],
      phone: json['phone'],
      password: json['password'],
      socialType: json['social_type'],
      socialId: json['social_id'],
      photo: json['photo'],
      bigPhoto: json['bigphoto'],
      active: json['active'],
      createdAt: json['created_at'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.userId;
    data['password'] = this.password;
    data['email'] = this.email;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['social_id'] = this.socialId;
    data['social_type'] = this.socialType;
    data['photo'] = this.photo;
    data['bigphoto'] = this.bigPhoto;
    data['active'] = this.active;
    data['created_at'] = this.createdAt;
    data['phone'] = this.phone;
    data['token'] = this.token;

    return data;
  }

  Map<String, dynamic> toJsonSignUp() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['email'] = this.email;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['phone'] = this.phone;
    return data;
  }

  Map<String, dynamic> toJsonUpdateProfile() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['last_name'] = this.lastName;
    data['phone'] = this.phone;
    return data;
  }


  Map<String, dynamic> toJsonNormalLogin() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['password'] = this.password;
    data['email'] = this.email;
    return data;
  }

  Map<String, dynamic> toJsonSocialLogin() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['email'] = this.email;
    data['first_name'] = this.firstName;
    data['last_name'] = this.lastName;
    data['social_id'] = this.socialId;
    data['social_type'] = this.socialType;
    data['photo'] = this.photo;
    return data;
  }

  @override
  String toString() {
    return 'toString_User {\n'
        'userId: $userId\n'
        'email: $email\n'
        'firstName: $firstName\n'
        // 'password: $password\n'
        'lastName: $lastName\n'
        'phone: $phone\n'
        'socialId: $socialId\n'
        'socialType: $socialType\n'
        'Active: $active\n'
        'cratedAt: $createdAt\n'
        'photo: $photo\n'
        'bigPhoto: $bigPhoto\n'
        'token: $token\n'
        '}';
  }
}
