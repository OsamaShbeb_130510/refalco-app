import 'package:get/get.dart';

class OrdersResponse {
  List<Order>? order;
  int? code;
  String? message;
  Paginate? paginate;

  OrdersResponse({this.order, this.code, this.message, this.paginate});

  OrdersResponse.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      order = <Order>[];
      json['data'].forEach((v) {
        order!.add(new Order.fromJson(v));
      });
    }
    code = json['code'];
    message = json['message'];
    paginate = json['paginate'] != null
        ? new Paginate.fromJson(json['paginate'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> Order = new Map<String, dynamic>();
    if (this.order != null) {
      Order['"data"'] = this.order!.map((v) => v.toJson()).toList();
    }
    Order['"code"'] = this.code;
    Order['"message"'] = '"${this.message}"';
    if (this.paginate != null) {
      Order['"paginate"'] = this.paginate!.toJson();
    }
    return Order;
  }
}

class Order {
  dynamic total;
  String? createdAt;
  String? image;
  String? currency;
  dynamic id;
  Address? address;
  RxBool isWished = false.obs;
  List<Items>? items;

  Order(
      {this.total,
        this.createdAt,
        this.image,
        this.currency,
        this.id,
        this.address,
        this.items});

  Order.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    createdAt = json['created_at'];
    image = json['image'];
    currency = json['currency'];
    id = json['id'];
    address =
    json['address'] != null ? new Address.fromJson(json['address']) : null;
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(new Items.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> Order = new Map<String, dynamic>();
    Order['"total"'] = this.total;
    Order['"created_at"'] = '"${this.createdAt}"';
    Order['"image"'] = '"${this.image}"' ;
    Order['"currency"'] = '"${this.currency}"' ;
    Order['"id"'] = this.id;
    if (this.address != null) {
      Order['"address"'] = this.address!.toJson();
    }
    if (this.items != null) {
      Order['"items"'] = this.items!.map((v) => v.toJson()).toList();
    }
    return Order;
  }

}

class Address {
  dynamic lat;
  dynamic lng;

  Address({this.lat, this.lng});

  Address.fromJson(Map<String, dynamic> json) {
    lat = json['lat'];
    lng = json['lng'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> Order = new Map<String, dynamic>();
    Order['"lat"'] = this.lat;
    Order['"lng"'] = this.lng;
    return Order;
  }
}

class Items {
  int? id;
  String? name;
  String? price;

  Items({this.id, this.name, this.price});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    price = json['price'].toString();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> Order = new Map<String, dynamic>();
    Order['"id"'] = this.id;
    Order['"name"'] = '"${this.name}"';
    Order['"price"'] = this.price;
    return Order;
  }
}

class Paginate {
  int? total;
  int? perPage;

  Paginate({this.total, this.perPage});

  Paginate.fromJson(Map<String, dynamic> json) {
    total = json['total'];
    perPage = json['per_page'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> Order = new Map<String, dynamic>();
    Order['"total"'] = this.total;
    Order['"per_page"'] = this.perPage;
    return Order;
  }
}
