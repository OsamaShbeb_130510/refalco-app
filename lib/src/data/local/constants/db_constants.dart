class DBConstants {
  DBConstants._();

  // Store Name
  static const String STORE_NAME = 'demo';

  // DB Name
  static const DB_NAME = 'bookthelook.db';

  // Fields
  static const FIELD_ID = 'id';
}
