import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:Refalco/src/ui/screens/homePage/home_page.dart';

import '../ui/screens/orderDetailsPage/order_details_page.dart';


class Routes {
  Routes._();

  static final routes = <String, WidgetBuilder>{
    HomePage.homePageRoute: (BuildContext context) => HomePage(),
  };

  static final List<GetPage>? getPages = [
    GetPage(
      name: HomePage.homePageRoute,
      transition: Transition.fadeIn,
      page: () => HomePage(
      ),
    ),
    GetPage(
      name: OrderDetailsPage.orderDetailsPageRoute,
      transition: Transition.fadeIn,
      page: () => OrderDetailsPage(),
    ),

  ];

  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      // case HomePage.homePageRoute:
      //   return MaterialPageRoute(builder: (_) {
      //     return HomePage(
      //       title: "Hello from OnGenrateRoute",
      //     );
      //   });
      //   break;
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }




}
