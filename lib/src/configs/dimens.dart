import 'package:flutter/cupertino.dart';

class Dimens {
  static const card_category_height = 120.00;
  static const card_category_width = 83.00;

  //for all screens

  static const double main_start_margin = 14.0;
  static const double padding_signup_page_start =18.0;
  static const double card_deals_width = 251.0;
  static const double card_deals_height = 194.0;
  static const double Card_offers_height = 100.0;

  static const double sub_header_fontsize = 16.0;
  static const double caption_fontsize = 16.0;

  static const double horizontal_padding = 12.0;
  static const double horizontal_mini_padding = 5.0;
  static const double horizontal_both_padding = 6.0;
  static const double vertical_padding = 12.0;
  static const double vertical_mini_padding = 5.0;

  static const card_mini_height = 112.0;
  static const fontSize22 = 22.0;
  static const fontSize18 = 18.0;
  static const fontSize14 = 14.0;

  double screenHeight(BuildContext context) {
    return MediaQuery.of(context).size.height;
  }
  double screenWidth(BuildContext context) {
    return MediaQuery.of(context).size.width;
  }

  Dimens._();
}