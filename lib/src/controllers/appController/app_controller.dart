import 'dart:convert';
import 'dart:typed_data';
import 'package:Refalco/src/data/models/orderModel/orders_response_model.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:Refalco/src/data/models/user/user.dart';
import 'package:Refalco/src/data/repository.dart';
import 'package:Refalco/src/ui/screens/homePage/home_page.dart';



class AppController extends GetxController {

  final Repository repository = Get.find();

 static AppController controller = Get.find();

  // var photoResponseLiveData = ApiResponse<MoviesResponse>.loading("message").obs;

  RxBool isUserLoggedIn = false.obs;

  List<int> wishList = [];

  AppController();

 // String filePath = Get.arguments;

  @override
  void onInit() async{
    //fetchMovieList();
    // await Firebase.initializeApp();
    repository.getUser().then((user) {
      if (user.token != null) {
        print('token:${user.token}');
        isUserLoggedIn.value = true;
      } else {
        isUserLoggedIn.value = false;
        print('token:${user.token}');

      }
    });
    print('AppGlobalController_init');
    print('WishListLength: ${await repository.wishList}');
    wishList = json.decode(await repository.wishList).cast<int>().toList();
    print('jsonDecode: ${jsonDecode(await repository.wishList)}');
    print('WishListLength: ${wishList.length}');

    super.onInit();
  }
/////////
  ////////////

  void removeFromWishList(Order? orderModel) {
      wishList.removeWhere((element) => element == int.parse(orderModel!.id!.toString()));
      print('WishListLenghth ${wishList.length} \n ${wishList.toString()}');
      repository.saveWishList(wishList.toString());
  }

  void addToWishList(Order?  orderModel) {
      wishList.add(int.parse(orderModel!.id!.toString()));
      // repository.box.write('Wish', wishList.toString());
      repository.saveWishList(wishList.toString());
      print('WishListLenghth ${wishList.length} \n ${wishList.toString()}');
  }

}