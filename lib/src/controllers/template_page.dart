import 'package:Refalco/src/configs/dimens.dart';
import 'package:Refalco/src/configs/strings.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class TemplatePage extends StatelessWidget{
  // static const String salonDetailsPageRoute = "/salonDetailsPage";
  // final HomeController controller = Get.find();
  final String? title ='';

  @override
  Widget build(BuildContext context) {
    return  SafeArea(
      top: false,
      bottom: false,
      child: Scaffold(
        // appBar: AppBarDefault(),
        body: Container(
          margin: EdgeInsetsDirectional.only(
              start: Dimens.main_start_margin, end: Dimens.main_start_margin),
          child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Template Page',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                      color: Color(0xff000000),
                    ),
                  ),
                  SizedBox(
                    height: 48,
                  ),

                ],
              )),
        ),
        // bottomNavigationBar: buildUpdateButton(context),
      ),
    );
  }
}

