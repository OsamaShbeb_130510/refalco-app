import 'package:Refalco/src/data/sheredpref/shared_preference_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:Refalco/src/configs/app_theme.dart';
import 'package:Refalco/src/configs/lacalization/localization.dart';
import 'package:Refalco/src/configs/routes.dart';
import 'package:Refalco/src/di/bindings/app_binding.dart';
import 'package:Refalco/src/ui/screens/homePage/home_page.dart';

class App extends StatelessWidget {


  App() : super();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
//      statusBarBrightness: Platform.isAndroid ? Brightness.light : Brightness.light,
      systemNavigationBarColor: Colors.white,
      systemNavigationBarDividerColor: Colors.grey,
      systemNavigationBarIconBrightness: Brightness.dark,
    ));

    return Material(
      child: GetMaterialApp(
        debugShowCheckedModeBanner: false,
          debugShowMaterialGrid: false,color: Colors.transparent,
          title: "Refalco",
          getPages: Routes.getPages,
          initialBinding: AppBindings(),
          //locale: currentAppLanguage,
          translations: Localization(),
          // locale: Get.deviceLocale,
          locale: Locale(Get.find<SharedPreferenceHelper>().currentLanguage),
          theme: AppTheme.lightTheme(),
          // initialRoute: SplashScreenPage.splashScreenRoute,
          initialRoute: HomePage.homePageRoute,
          onGenerateRoute: Routes.generateRoute,
          // routes: Routes.routes
      ),
    );

  }
}
