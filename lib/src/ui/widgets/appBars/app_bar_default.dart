import 'package:Refalco/src/configs/colors.dart';
import 'package:flutter/material.dart';

class AppBarDefault extends StatelessWidget implements PreferredSizeWidget{

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: AppColors.lightBG,
      automaticallyImplyLeading: false,
      elevation: 0,
//      leading: SizedBox(child: Icon(Icons.arrow_back,color: Colors.white,),
//        height: 4,width: 4,),
      title: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          InkWell(
            child: SizedBox(
              child: Icon(
                Icons.keyboard_backspace,
                color: Colors.black,
              ),
            ),
            onTap: ()=>{
              Navigator.pop(context)
            },
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => new Size.fromHeight(AppBar().preferredSize.height);

}
