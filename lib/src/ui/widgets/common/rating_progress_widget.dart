// import 'package:flutter/material.dart';
// import 'package:percent_indicator/linear_percent_indicator.dart';
//
// class RatingProgressWidget extends StatelessWidget {
//
//   final String? skill;
//    var valueLevel;
//   final Color? backgroundColor;
//   final Color? progressColor;
//
//   RatingProgressWidget({this.skill, this.valueLevel,this.backgroundColor,
//     this.progressColor});
//
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Row(
//         children: <Widget>[
//           SizedBox(width: 0.0),
//           Expanded(
//               flex: 1,
//               child: new Text(
//                 skill ?? "0 stars",
//                 style: TextStyle(
//                   fontSize: 10,
//                   color: Color(0xff000000),
//                 ),
//               )),
//           SizedBox(width: 1.0),
//           Expanded(
//             flex: 4,
//             child: Container(
//               child: Padding(
//                 padding: EdgeInsets.all(4.0),
//                 child: LinearPercentIndicator(
//                   animation: false,
//                   lineHeight: 8.0,
//                   animationDuration: 2000,
//                   percent: valueLevel.toDouble()/100,
//                   linearStrokeCap: LinearStrokeCap.roundAll,
//                   progressColor: progressColor ?? Color(0xff415E24),
//                   backgroundColor: backgroundColor ?? Color(0xffF2F2F2),
//                 ),
//               ),
//             ),
//           ),
//           Expanded(
//               flex: 1,
//               child: Text(
//                 "${valueLevel.toStringAsFixed(1)}%",
//                 maxLines: 1,
//                 overflow:TextOverflow.ellipsis,
//                 style: TextStyle(
//                   fontSize: 10,
//                   color: Color(0xff000000),
//                 ),
//               )),
//         ],
//       ),
//
//     );
//   }
//
// }
