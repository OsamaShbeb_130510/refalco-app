import 'package:Refalco/src/configs/dimens.dart';
import 'package:Refalco/src/controllers/appController/app_controller.dart';
import 'package:Refalco/src/ui/screens/orderDetailsPage/order_details_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import '../../../configs/colors.dart';
import '../../../data/models/orderModel/orders_response_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';

import '../../../data/repository.dart';

class CardAllOrders extends StatelessWidget {
  final Order? orderModel;
  final Function(Order)? onTap;

    CardAllOrders({Key? key, required this.onTap, required this.orderModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    var box = GetStorage();
    return InkWell(
      onTap: () {
        onTap!(orderModel!);
      },
      child: Padding(
        padding: const EdgeInsets.all(Dimens.horizontal_both_padding),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xffECEDEF),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: AppColors.primaryColor),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: Dimens.horizontal_padding,
                vertical: Dimens.vertical_mini_padding),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: Get.height * 210 / 896,
                  width: Get.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: CachedNetworkImage(
                        imageUrl: orderModel!.image!,
                        //${productModel!.img1.toString()
                        fit: BoxFit.cover,
                        errorWidget: (context, url, error) => Icon(
                          Icons.error,
                          color: AppColors.primaryColorDark,
                        ),
                        placeholder: (context, url) {
                          return Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: CircularProgressIndicator(
                                strokeWidth: 1,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                    Colors.black12),
                              ),
                            ),
                          );
                        },
                      )),
                ),
                SizedBox(
                  height: Dimens.vertical_padding,
                ),
                Text("# ${orderModel!.id}"),
                SizedBox(
                  height: Dimens.vertical_padding,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(
                          orderModel!.total!.toString(),
                          style: TextStyle(
                              fontSize: Dimens.fontSize18, fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          width: Dimens.horizontal_mini_padding,
                        ),
                        Text(
                          "${orderModel!.currency}",
                          style: TextStyle(
                              fontSize: Dimens.fontSize14,
                              color: Colors.black.withOpacity(0.50)),
                        ),
                      ],
                    ),
                    handleWishState()
                  ],
                ),
                SizedBox(
                  height: Dimens.vertical_padding,
                ),
                Text(DateFormat()
                    .format(DateTime.parse(orderModel!.createdAt!).toLocal())),
                SizedBox(
                  height: Dimens.vertical_padding * 2,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  handleWishState() {
    return Obx(() {
      return orderModel!.isWished.value
          ? InkWell(
          onTap: (){
            AppController.controller.removeFromWishList(orderModel);
            orderModel!.isWished.value = !orderModel!.isWished.value;
          },
          child: Icon(Icons.favorite, color: Colors.red))
          : InkWell(
          onTap: (){
            AppController.controller.addToWishList(orderModel);
            orderModel!.isWished.value = !orderModel!.isWished.value;
          },
          child: Icon(Icons.favorite_border));
    });


  }
}

