import 'package:Refalco/src/data/models/orderModel/orders_response_model.dart';
import 'package:Refalco/src/ui/widgets/cards/card_all_order.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../data/models/api_response.dart';
import '../../widgets/common/error_widget.dart';
import '../../widgets/common/pagination/pagination_list_widget.dart';
import '../orderDetailsPage/order_details_page.dart';

class OrderPaginationWidget extends StatelessWidget {
  final Rx<ApiResponse<OrdersResponse>>? productLiveData;
  final Rx<ApiResponse<OrdersResponse>>? paginationLiveData;
  final Function? onRetryPressed;
  final Function? onPaginationRetryClick;

  OrderPaginationWidget(
      {@required this.productLiveData,
      @required this.onRetryPressed,
      @required this.paginationLiveData,
      @required this.onPaginationRetryClick,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      if (!productLiveData!.value.isNull) {
        switch (productLiveData!.value.status) {
          case Status.LOADING:
            return Container(
                height: Get.height * 0.8,
                child: const Center(child: LoadingWidget()));
            break;
          //carousel_slider
          case Status.COMPLETED:
            if (productLiveData!.value.data!.isNull) {
              return Center(
                child: Container(
                  margin: EdgeInsets.all(30),
                  child: Text('No Result'),
                ),
              );
            }
            return buildContentPagination(productLiveData!.value.data!.order!);
          case Status.ERROR:
            return SizedBox(
              height: Get.height * 0.8,
              child: Center(
                child: ErrorsWidget(
                  onRetryPressed: () => {onRetryPressed!()},
                  errorMessage: '',
                ),
              ),
            );
            break;
        }
      }
      return LoadingWidget();
    });
  }

  void moveToDetailsPage(Order order) {
     Get.toNamed(OrderDetailsPage.orderDetailsPageRoute, arguments: order);
  }

  buildContentPagination(List<Order> orders) {
    ///to reverse the list
    List<Order>? list = productLiveData!.value.data!.order;

    ////////////
    return PaginationListWidget<OrdersResponse, Order>(
        itemsList: orders,
        paginationLiveData: paginationLiveData,
        loadingWidget: LoadingWidget(),
        onRetryClick: () {
          onPaginationRetryClick!();
        },
        itemBuilder: (context, index) {
          return
                CardAllOrders(
                orderModel: list![index],
                onTap: (order) {
                  moveToDetailsPage(order);
                },
              )
              ;
        });
  }
}

class LoadingWidget extends StatelessWidget {
  final String? loadingMessage;

  const LoadingWidget({ Key? key, this.loadingMessage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: CircularProgressIndicator(),
    );
  }
}
