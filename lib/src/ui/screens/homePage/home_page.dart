import 'package:Refalco/src/configs/colors.dart';
import 'package:Refalco/src/ui/screens/homePage/home_controller.dart';
import 'package:Refalco/src/ui/screens/homePage/order_pagination_widget.dart';
import 'package:Refalco/src/ui/widgets/appBars/app_bar_default.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../configs/dimens.dart';
import '../../../configs/strings.dart';

class HomePage extends StatelessWidget {
  static const String homePageRoute = "/";
  final HomeController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SafeArea(
        top: true,
        bottom: false,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: AppColors.nearlyBlue.withOpacity(0.1),
            title: Center(
              child: Text(
                Strings.appName.tr,
                style: (TextStyle(fontSize: Dimens.fontSize22, fontWeight: FontWeight.bold)),
              ),
            ),
          ),
          body: RefreshIndicator(
            onRefresh: controller.onRefresh,
            child: SingleChildScrollView(
              controller: controller.scrollController,
              child: Column(
                children: [buildOrdersList()],
              ),
            ),
          ),
        ),
      ),
    );
  }

  buildOrdersList(){
    return OrderPaginationWidget(
      productLiveData: controller.orderLiveData,
      paginationLiveData: controller.paginationLiveData,
      onRetryPressed: () {
        controller.fetchOrderList();
      },
      onPaginationRetryClick: (){
        controller.getNewPage();
      },
    );
  }
}
