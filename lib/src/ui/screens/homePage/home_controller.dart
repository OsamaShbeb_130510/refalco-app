import 'dart:convert';

import 'package:Refalco/src/controllers/appController/app_controller.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:Refalco/src/data/repository.dart';
import '../../../data/models/api_response.dart';
import '../../../data/models/orderModel/orders_response_model.dart';
import '../../../utils/dio/dio_error_util.dart';


class HomeController extends GetxController{

  final Repository repository = Get.find();
  ScrollController scrollController = ScrollController();
  bool isLoading=false;
  int page=0;
  int limit=15;
 OrdersResponse ordersList=OrdersResponse();


  var orderLiveData = ApiResponse<OrdersResponse>.loading("message").obs;
  var paginationLiveData = ApiResponse<OrdersResponse>.completed(null).obs;


  HomeController();

  @override
  void onInit() async {
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.position.pixels) {
        print("EndScrolling");

          getNewPage();

      }
    });
    print('OSAMA ${await repository.orderList}');
    fetchOrderList();
    super.onInit();
  }
  Future<void> onRefresh() async {
    fetchOrderList();
  }

  @override
  void dispose() {
    super.dispose();
  }

  fetchOrderList() async {
    page=1;
     orderLiveData.value = ApiResponse<OrdersResponse>.loading('Fetching orders');
    try {
      OrdersResponse orders = await repository.getAllOrders(page,limit);
        orderLiveData.value = ApiResponse<OrdersResponse>.completed(orders);
        ordersList=orders;
        handleWishList(ordersList);
        repository.saveOrderList(orders.toJson().toString());
        print('OSAMA ${orders.toJson()}');
    }
    on DioError catch (error, stacktrace) {
      String? localOrdersString = await repository.orderList;
      if (localOrdersString != null) {
        OrdersResponse orders =OrdersResponse.fromJson(json.decode(localOrdersString));
        print("LOCALORDER ${orders.order!.length}");
        handleWishList(orders);
        orderLiveData.value = ApiResponse<OrdersResponse>.completed(orders);
        ordersList=orders;
      }else{
        orderLiveData.value =
            ApiResponse.error(DioErrorUtil.handleError(error));
      }


     print('$error $stacktrace');
    //  toastMessage(DioErrorUtil.handleError(error) ?? "Error");
    }
    catch (error, stacktrace) {
      orderLiveData.value=
      ApiResponse<OrdersResponse>.error(error.toString());
      print('$error $stacktrace');
    }
  }
  getNewPage() async {
    if (!isLoading) {
      page++;
      print('getNewPage$page');
      paginationLiveData.value =
      ApiResponse<OrdersResponse>.loading('fetching');
      isLoading = !isLoading;
      try {
        OrdersResponse? ordersResponse =
        await repository.getAllOrders(page,limit);

        ordersList.order!.addAll(ordersResponse.order!);
        orderLiveData.value = ApiResponse.completed(ordersList);
        paginationLiveData.value = ApiResponse.completed(ordersList);

        handleWishList(ordersList);

        isLoading = !isLoading;
      } on DioError catch (error, stacktrace) {
        paginationLiveData.value =
        ApiResponse<OrdersResponse>.error(DioErrorUtil.handleError(error));
        page--;
        isLoading = !isLoading;
        print("Err$error$stacktrace");
      } catch (error, stacktrace) {
        paginationLiveData.value =
        ApiResponse<OrdersResponse>.error(error.toString());
        page--;
        isLoading = !isLoading;
        print('Err$error$stacktrace');
      }
    }
  }

  void handleWishList(OrdersResponse ordersList) {

    ordersList.order!.forEach((order) {

      int? foundOrder = AppController.controller.wishList.firstWhereOrNull((element) => element == int.parse(order.id!.toString()),);
      if(foundOrder != null ){
        order.isWished.value = true;
      }else{
        order.isWished.value = false;
      };

    });
  }

}
