

import 'package:Refalco/src/data/models/orderModel/orders_response_model.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';

import '../../../data/repository.dart';
import '../../../utils/local_notification.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

class  OrderDetailsController extends GetxController{
  final Repository repository = Get.find();
  Order order=Get.arguments;
  OrderDetailsController();

  @override
  void onInit() {
    NotificationApi.initialize(flutterLocalNotificationsPlugin);

    super.onInit();
  }

  @override
  void onClose() {
    super.onClose();
  }

  void showNotiication() {
    NotificationApi.showText(
        title: 'liked',
        body: 'added to favorite list',
        fln: flutterLocalNotificationsPlugin);
  }



}
