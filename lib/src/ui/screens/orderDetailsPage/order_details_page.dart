import 'package:Refalco/src/configs/colors.dart';
import 'package:Refalco/src/controllers/appController/app_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import '../../../configs/dimens.dart';
import '../../../utils/local_notification.dart';
import 'order_details_controller.dart';
import 'package:audioplayers/audioplayers.dart';

class OrderDetailsPage extends StatelessWidget {
  static const String orderDetailsPageRoute = "/orderDetailsPageRoute";
  final OrderDetailsController controller = Get.find();

  OrderDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              buildGoogleMapWidget(),
              buildOrderItems(),
              SizedBox(
                height: Dimens.vertical_padding,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    controller.order.total!.toString(),
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: AppColors.nearlyBlue),
                  ),
                  SizedBox(
                    width: Dimens.horizontal_mini_padding,
                  ),
                  Text(
                    "${controller.order.currency}",
                    style: TextStyle(
                        fontSize: 14, color: Colors.black.withOpacity(0.50)),
                  ),
                ],
              ),
              SizedBox(
                height: Dimens.vertical_padding,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(
                    () {
                      return controller.order.isWished.value
                          ? InkWell(
                              onTap: () {
                                AppController.controller
                                    .removeFromWishList(controller.order);
                                controller.order.isWished.value =
                                    !controller.order.isWished.value;
                                final player = AudioPlayer();
                                player.play(
                                    AssetSource('sounds/sound_shutter.mp3'));
                              },
                              child: Icon(
                                Icons.favorite,
                                color: Colors.red,
                                size: 33,
                              ),
                            )
                          : InkWell(
                              onTap: () {
                                AppController.controller
                                    .addToWishList(controller.order);
                                controller.order.isWished.value =
                                    !controller.order.isWished.value;
                                controller.showNotiication();
                              },
                              child: Icon(
                                Icons.favorite_border,
                                color: Colors.red,
                                size: 33,
                              ),
                            );
                    },
                  ),
                ],
              ),
              SizedBox(
                height: Dimens.vertical_padding,
              )
            ],
          ),
        ),
      ),
    );
  }

  buildGoogleMapWidget() {
    Set<Marker> myMarker = {
      Marker(
        markerId: MarkerId(controller.order.id.toString()),
        position: LatLng(
            double.parse(controller.order.address!.lat!.toString()),
            double.parse(controller.order.address!.lng!.toString())),
      )
    };
    return Container(
      child: GoogleMap(
        markers: (myMarker),
        initialCameraPosition: CameraPosition(
          target: LatLng(
              double.parse(controller.order.address!.lat!.toString()),
              double.parse(controller.order.address!.lng!.toString())),
          //   LatLng(35.6130, 36.0023),
          zoom: 12,
        ),
      ),
      height: Get.height * 0.8,
      color: Colors.grey,
    );
  }

  buildOrderItems() {
    return ListView.builder(
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        itemCount: controller.order.items!.length,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.symmetric(
                vertical: Dimens.vertical_mini_padding,
                horizontal: Dimens.vertical_mini_padding),
            child: Row(
              children: [
                Text(
                  controller.order.items![index].name!,
                ),
                SizedBox(
                  width: Dimens.horizontal_mini_padding,
                ),
                Text(
                  controller.order.items![index].price!,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          );
        });
  }


}
