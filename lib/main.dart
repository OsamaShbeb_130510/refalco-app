import 'package:Refalco/src/app.dart';
import 'package:Refalco/src/di/bindings/async_bindings.dart';
// import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';


//build release apk
//flutter build apk --target-platform android-arm,android-arm64,android-x64 --split-per-abi

Future<void> main() async {

  WidgetsFlutterBinding.ensureInitialized();

  await SystemChrome.setPreferredOrientations(<DeviceOrientation>[
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
//    DeviceOrientation.landscapeRight,
//    DeviceOrientation.landscapeLeft
  ]).then((_) async {
    // appComponent = await AppComponent.create(
    //   NetworkModule(),
    //   LocalModule(),
    //   PreferenceModule(),
    // );
    if(GetPlatform.isWeb){
      //todo add firebase configuration for web
    }else{
      // await Firebase.initializeApp();
    }
    await AsyncBindings().dependencies();
    await GetStorage.init();
    runApp(App());
  });
}
